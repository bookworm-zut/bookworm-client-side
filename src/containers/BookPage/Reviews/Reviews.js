import React, { Component } from 'react';

import Review from '../../../components/BookPage/Reviews/Review/Review'
import Button from '../../../components/UI/Button/Button'
import MessageBox from '../../../components/UI/MessageBox/MessageBox'
import StarRating from '../../../components/UI/StarRating/StarRating'
import Modal from '../../../components/UI/Modal/Modal'
import Section from '../../../components/UI/Section/Section'

import axios from '../../../axios-requests';

import classes from './Reviews.module.css';

class Reviews extends Component {
    state = {
        writingReview: false,
        bookId: this.props.bookId,
        messageBoxValue: '',
        isReviewSent: false,
        isReviewAdded: false
    }

    onWriteReviewClicked = () => {
        this.setState({ writingReview: true });
    }

    onWriteReviewCancel = () => {
        this.setState({ writingReview: false, isReviewSent: false, isReviewAdded: false});
    }

    onMessageBoxInputChanged = (e) => {
        this.setState({ messageBoxValue: e.target.value });
    }

    onMessageBoxInputChanged = (e) => {
            this.setState({ messageBoxValue: e.target.value });
    }

    onSubmitReview = (e) => {
        axios.post(`/books/addBookReview/${this.state.bookId}`, this.state.messageBoxValue, { headers: { 'Content-Type': 'application/json' }})
            .then(res => {
                this.setState({writingReview: true, messageBoxValue: '',isReviewSent: true, isReviewAdded: true});  
            })
            .catch(err => {
                this.setState({writingReview: true, messageBoxValue: '', isReviewSent: true, isReviewAdded: false});  
            })
    
        e.preventDefault();
    }

    sortAndMapReviews() {
        let reviews = this.props.reviews.sort((a, b) => (
            new Date(b.timeOfCreation).getTime() - new Date(a.timeOfCreation).getTime()
        ));

        reviews = this.props.reviews.map(review => {
            return <Review 
                    key={review.id}
                    username={review.reviewAuthor.username}
                    review={review.reviewText}
                    upvotes={review.upvotes}
                    downvotes={review.downvotes}
                    bookId={this.state.bookId}
                    date={new Date(review.timeOfCreation)}
                />
        });
        return reviews;
    }

    prepareReviewsDisplay() {
        let reviews = null;
        if (this.props.reviews.length !== 0) {
            reviews = this.sortAndMapReviews();
        } else {
            reviews = <h2 style={{color: "rgba(33, 137, 255, 1)", padding: "1rem 1rem", textAlign: "center"}}>Be the first one to review this book!</h2>;
        }
        return reviews;
    }

    prepareReviewAddingStatus() {
        let status = null;
        let classList = [classes.Status];
        if (this.state.isReviewSent && this.state.isReviewAdded) {
            classList.push(classes.Success)
            status = (
                <div className={classList.join(' ')}>
                    <p>Review added!</p>
                </div>
            )
        } else if (this.state.isReviewSent) {
            classList.push(classes.Danger)
            status = (
                <div className={classList.join(' ')}>
                    <p>Error occured when adding review.</p>
                </div>
            )
        }
        return status;
    }

    prepareAddReviewButton() {
        let addReviewBtn = null;
        if (this.props.userId) {
            addReviewBtn = (<div style={{textAlign: "center"}}>
                <Button style={{width: "50%"}} clicked= {() => this.onWriteReviewClicked()}>Write a  review!</Button>
            </div>)     
        } else {
            addReviewBtn = (<div style={{textAlign: "center"}}>
                <Button style={{width: "50%"}} disabled>Log in to add a review</Button>
            </div>)     
        }
        return addReviewBtn;
    }

    prepareReviewBox() {
        const inputCfg = {
            height: "10rem",
            margin: "0"
        }

        let reviewBox = null
        if (this.state.writingReview) {
            reviewBox = (
                <MessageBox
                    title="Write a review"
                    inputConfig={inputCfg}
                    value={this.state.messageBoxValue}
                    changed={(e) => this.onMessageBoxInputChanged(e)}
                    btnClicked={(e) => this.onSubmitReview(e)}>
                    <p>Your rating: </p>
                    <StarRating
                        readonly
                        userId={this.props.userId}
                        bookId={this.state.bookId} />
                </MessageBox>
            )
        }
        return reviewBox;
    }

    render() { 
        let reviewBox = this.prepareReviewBox();
        let reviews = this.prepareReviewsDisplay();
        let status = this.prepareReviewAddingStatus();
        let addReviewBtn = this.prepareAddReviewButton();

        return (
            <div>
                <Modal show={this.state.writingReview} modalClosed={() => this.onWriteReviewCancel()}>
                    {reviewBox}
                    {status}
                </Modal>
                <Section title="Reviews for this book"> 
                    {addReviewBtn}
                    <div className={classes.ReviewsCard}>   
                        {reviews}
                    </div>
                </Section>
            </div>
        )
    }
}

export default Reviews;