import React, { Component } from 'react'
import { connect } from 'react-redux';

import Showcase from '../../components/UI/Showcase/Showcase'
import BookInfo from '../../components/BookPage/Showcase/BookInfo/BookInfo'
import BookRatingPanel from '../../components/BookPage/Showcase/BookRatingPanel/BookRatingPanel';
import Description from '../../components/BookPage/Description/Description';
import Reviews from './Reviews/Reviews'

import Aux from '../../hoc/Auxiliary'
import Spinner from '../../components/UI/Spinner/Spinner'

import * as actions from '../../store/actions/index'
import classes from './BookPage.module.css';
import image from '../../assets/BookPageShowcaseBackground.jpg'

class BookPage extends Component {
    state = {
        writingReview: false,
    }

    componentDidMount() {
        const query = new URLSearchParams(this.props.match.params);
        let bookId = query.get('id');
   
        if (bookId) {   //do zmiany jak sie dowiem jak wyglada id
            this.props.onFetchBook(`${bookId}`); //tu oczywiscie bookId
        }  
    }

    render() {      
        let page = <Spinner />;
        if (!this.props.loading && this.props.book && this.props.book.meta && this.props.reviews) {
            console.log(this.props.book.meta);
            const book = {
                id: this.props.book.id,
                name: this.props.book.name,
                author: this.props.book.author,
                description: this.props.book.description,
                image: this.props.book.image,
                meta: {
                    isbn: this.props.book.meta.isbn,
                    genre: this.props.book.meta.genre,
                    rating: parseFloat(this.props.book.meta.rating),
                    released: this.props.book.meta.released
                },
                reviews: this.props.reviews
            }

            page = (
                <Aux>
                    <Showcase
                        size="normal"
                        backgroundImage={image}
                        left={<BookInfo
                            image={book.image}
                            bookName={book.name}
                            author={book.author}
                            isbn={book.meta.isbn} 
                            genre={book.meta.genre} 
                            released={book.meta.released} 
                            />}
                        right={<BookRatingPanel
                            book={book.id}    
                            rating={book.meta.rating}
                            ratingsAmount={book.meta.ratingsAmount}
                            userId={this.props.userId} />}>
                    </Showcase>
                    <div className={classes.Page}>
                        <div className={classes.Left}>
                            <Reviews
                            bookId={book.id}
                            userId={this.props.userId}
                            reviews={[...book.reviews]}
                            writeReview={() => this.onWriteReviewClicked()} />
                        </div> 
                        <div className={classes.Right}>
                            <Description description={book.description}/>
                        </div>
                    </div>   
                </Aux>
            )
        }

        return (
            <Aux>
                {page}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.book.loadingInfo || state.book.loadingReviews,
        book: state.book.book,
        reviews: state.book.reviews,
        token: state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = (dispatch, state) => {
    return {
        onFetchBook: (bookName) => dispatch(actions.fetchBook(bookName))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookPage);
