import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://localhost:8189/'
});

export default instance;
