import React, { Component } from 'react';

import classes from './BookRatingPanel.module.css';

import StarRating from '../../../UI/StarRating/StarRating';
import Heading from '../../../UI/Text/Heading/Heading';

class BookRatingPanel extends Component {
    state = {
        loading: false
    }

    render() {
        let averageBookRatingText = <li>Current average book rating: {this.props.rating}/5.00</li>;
        if (isNaN(this.props.rating)) {
            averageBookRatingText = "No ratings yet. Be the first one to rate this book!"
        }
        return (
            <div className={classes.Panel}>
                <Heading size="medium" color="#ffc011" marginBottom="0.2rem">Rate this book!</Heading>
                <StarRating canBeReset 
                    bookId={this.props.book}
                    userId={this.props.userId}
                    size="2x"/>
                <ul className={classes.Ul}>
                   {averageBookRatingText}
                </ul>
            </div>
        );
    }
}

export default BookRatingPanel;