import React, { Component } from 'react';

import Section from '../../UI/Section/Section'
import * as classes from './Description.module.css'

class Description extends Component {
    state = {
        descriptionToDisplay: this.props.description,
        descriptionShort: null,
        isOriginalDescriptionDisplayed: false,
        isDescriptionChanged: false
    }

    changeDescriptionDisplay(showOriginal) {
        if (showOriginal) {
            this.setState({ isOriginalDescriptionDisplayed: true, descriptionToDisplay: this.props.description });
        } else {
            this.setState({ isOriginalDescriptionDisplayed: false, descriptionToDisplay: this.state.descriptionShort });
        }
    }

    componentDidMount() {
        //onClick={this.showOriginalDescription()}
        let description = this.props.description; 
        if (description === "No description") {
            description = "No description available for this book.";
        } else if (description.length > 350) {
            description = description.slice(0, 350);
            description = description + "... ";
            this.setState({ descriptionShort: description, isDescriptionChanged: true });
        }
        this.setState({ descriptionToDisplay: description });
    }

    render() {
        let show = null;
        if (this.state.isDescriptionChanged) {
            show = this.state.isOriginalDescriptionDisplayed ? 
                <span onClick={() => this.changeDescriptionDisplay(false)} className={classes.Show}>Show less</span> :
                <span onClick={() => this.changeDescriptionDisplay(true)} className={classes.Show}>Show more</span>
        }

        return (
            //<span className={classes.ShowMore}>Show more</span>
            <Section title="Book description">
                <p className={classes.Description}>{this.state.descriptionToDisplay}  
                &nbsp;{show}
                </p>
            </Section>
        );
    }
}

export default Description;