import React from 'react';

import User from '../../../UI/User/User';
import Heading from '../../../UI/Text/Heading/Heading';

const discussionBoxItem = (props) => {
    return (
        <div>
            <Heading small>{props.title}</Heading>
            Hosted by: <User noImage userName={props.userName} />
        </div>
    );
}

export default discussionBoxItem;