import React from 'react';
import classes from './AuthorInfo.module.css';
import authorImage from '../../../../assets/authoravatar.png'
import Image from '../../../UI/Image/Image'
import Heading from '../../../UI/Text/Heading/Heading'

const authorInfo = (props) => {
    return (
        <div className={classes.InfoContainer}>
            <Image src={authorImage} alt="mickiewicz" round size="large" />
            <Heading size="big">{props.name}</Heading>
        </div>
    );
}

export default authorInfo;