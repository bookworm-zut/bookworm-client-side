import React from 'react';

import Heading from '../../../UI/Text/Heading/Heading'

import classes from './AuthorBio.module.css'

const authorBio = (props) => {
    return (
        <div className={classes.Bio}>
            <Heading size="medium" style={{color: "gold"}}>About</Heading>
            <ul>
                <li>Books released: {props.booksReleasedAmount}</li>
                <li>Average book rating: {props.averageRating}/5.00</li>
            </ul>        
        </div>
    )
}

export default authorBio;