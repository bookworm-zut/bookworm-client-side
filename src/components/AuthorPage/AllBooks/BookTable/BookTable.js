import React from 'react';

import BookTableItem from './BookTableItem/BookTableItem'

import classes from './BookTable.module.css';

const bookTable = (props) => {
    const bookTableItems = props.books.map(book => {
        return <BookTableItem 
                image={book.thumbnailUrl}
                key={book.id}
                id={book.id}
                title={book.title}
                rating={book.bookAverageRate}
                released={book.publishedDate}
            />
    })
        
    return (
        <table className={classes.Table}>
            <thead className={classes.Thead}>
                <tr>
                    <th className={classes.Th}>Cover</th>
                    <th className={classes.Th}>Title</th>
                    <th className={classes.Th}>Rating</th>
                    <th className={classes.Th}>Released</th>
                </tr>
            </thead>
            <tbody className={classes.Tbody}>
                {bookTableItems}
            </tbody>
        </table>
    );
}

export default bookTable;

