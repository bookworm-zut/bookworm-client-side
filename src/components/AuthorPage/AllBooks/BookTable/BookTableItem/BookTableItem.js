import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import StarRating from '../../../../UI/StarRating/StarRating'
import Image from '../../../../UI/Image/Image'

import classes from './BookTableItem.module.css';

class BookTableItem extends Component {
    redirect() {
        this.props.history.push(`/book/${this.props.id}`);
    }

    render() {
        let rating = this.props.rating;
        if (rating === null) {
            rating = 0;
        }
        return (
            <tr className={classes.Tr}>
                <td className={classes.Td}><Image size="medium" src={this.props.image} alt="book" /></td>
                <td className={classes.Td} onClick={() => this.redirect()}>
                    {this.props.title}
                </td>
                <td className={classes.Td}>
                    <StarRating readonly rating={rating}/>
                </td>
                <td className={classes.Td}>{this.props.released}</td>
            </tr>
        );
    }
}

export default withRouter(BookTableItem);