import React from 'react';

import BookTable from './BookTable/BookTable'
import Section from '../../UI/Section/Section'

const allBooks = (props) => {
    return (
        <Section title={`All books by ${props.author}`}>
            <BookTable books={props.books}/>
        </Section>
    );
}

export default allBooks;