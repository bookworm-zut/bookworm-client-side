import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import Heading from '../../../UI/Text/Heading/Heading'
import Image from '../../../UI/Image/Image'

import classes from './TopBook.module.css';

class TopBook extends Component {
    redirect = () => {
        this.props.history.push(`/book/${this.props.book.id}`);
    }

    render() {
        let rating = this.props.book.bookAverageRate;
        if (rating === null) {
            rating = 0;
        }
        return (
            <div className={classes.TopBook} onClick={() => this.redirect()}>
                <div className={classes.BookImage}>
                    <Image size={this.props.size} src={this.props.book.thumbnailUrl} alt="topbook" />
                </div>
                <div className={classes.BookInfo}>
                    <Heading size="medium">{this.props.book.title}</Heading>
                    <ul>
                        <li><strong>Rating: </strong>{rating}/5.00</li>
                        <li><strong>Released: </strong>{this.props.book.publishedDate}</li>
                        <li><strong>Categories: </strong>{this.props.book.categories}</li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default withRouter(TopBook);