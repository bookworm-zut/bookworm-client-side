import React from 'react';

import Image from '../Image/Image'
import Heading from '../Text/Heading/Heading'

import classes from './User.module.css';

const user = (props) => {
    let image = null;
    if (!props.noImage) {
        image = (
            <Image 
                src={props.image} 
                alt={props.userName}   
                style={{marginRight: "0.5rem"}} 
                round />
        )
    }
    return (
        <div className={classes.Wrapper}>
            <div className={classes.User}>
                {image}
                <Heading><a className={classes.DisabledLink}>{props.userName}</a></Heading> 
            </div>
        </div>     
    );
}

export default user;