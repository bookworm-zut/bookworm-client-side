import React from 'react'
import Heading from '../Text/Heading/Heading'
import classes from './Section.module.css'

const section = (props) => (
    <section className={classes.Container}>  
        <Heading sectionTitle size="medium">{props.title}</Heading>
        {props.children}
    </section>
)

export default section;