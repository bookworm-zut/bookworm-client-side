import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import Input from '../Input/Input';
import Button from '../Button/Button';
import classes from './SearchBar.module.css';

class SearchBar extends Component {
    state = {
        categoryConfig: {
            options: [
                { value: 'bookName', displayValue: 'Books' },
                { value: 'authorName', displayValue: 'Authors' },
                { value: 'userName', displayValue: 'Users' },
            ]
        },
        queryConfig: {
            type: 'text',
            placeholder: 'Search...'
        },
        category: 'bookName',
        query: '',
        toSearchPage: false
    };

    componentDidUpdate() {
        if (this.state.toSearchPage) {
            this.setState({toSearchPage: false});
        }
    }

    categoryChangedHandler = event => {
        this.setState({ category: event.target.value });
    }

    queryChangedHandler = event => {
        this.setState({ query: event.target.value });
    }

    submitHandler = event => {
        event.preventDefault();
        if (this.state.query !== '') {
            this.setState({toSearchPage: true});
        }
    }

    render() {
        if (this.state.toSearchPage) {
            return <Redirect push to={{
                pathname: '/search',
                search: '?' + this.state.category + '=' + this.state.query
            }} />;
        }

        return (
            <form onSubmit={this.submitHandler}>
                <div className={classes.SearchBar}>
                    <Input
                        key="searchType"
                        elementType="select"
                        elementConfig={this.state.categoryConfig}
                        value={this.state.category}
                        changed={this.categoryChangedHandler} />
                    <div>
                        <Input
                            key="searchText"
                            elementType="text"
                            elementConfig={this.state.queryConfig}
                            value={this.state.query}
                            changed={this.queryChangedHandler} />
                        <Button btnType="Success">
                            <FontAwesomeIcon icon={faSearch} />
                        </Button>
                    </div>
                </div>
            </form>
        );
    }
}

export default SearchBar;
