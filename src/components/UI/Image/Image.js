import React from 'react';

import classes from './Image.module.css'

const image = (props) => {
    let classList = [classes.Img];
    let style = {
        ...props.style,
    };
    let height;
    if (props.size === "large") {
        classList.push(classes.large);
        height = "150px";
    } 
    else if (props.size === "big") {
        classList.push(classes.big);
        height = "125px";
    }
    else if (props.size === "medium") {
        classList.push(classes.medium);
        height = "100px";
    }
    else if (props.size === "small"){
        classList.push(classes.small);
        height = "70px";
    }
    else {
        classList.push(classes.xsmall);
        height = "50px";
    }

    if (props.round) {
        classList.push(classes.round);
        style = {
            ...style,
            height: height
        };
    }
    
    if (props.noshadow) {
        style = {
            ...style,
            boxShadow: "0px 0px 0px 0px rgba(0,0,0,0.75)"
        }
    }

    return (
        <img src={props.src} alt={props.alt} className={classList.join(' ')} style={style} />
    )
}

export default image;