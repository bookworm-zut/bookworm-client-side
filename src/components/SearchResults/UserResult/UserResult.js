import React from 'react';

import Heading from '../../UI/Text/Heading/Heading';
import Image from '../../UI/Image/Image';

import classes from './UserResult.module.css';

const userResult = props => {
    return (
        <div className={classes.UserResult} onClick={props.onClick}>
            <div className={classes.UserImage}>
                <Image size="medium" src={props.image} alt="user" />
            </div>
            <div className={classes.UserInfo}>
                <Heading size="big">{props.fullname}</Heading>
                <Heading size="small">{props.username}</Heading>
                <h5>{props.descr}</h5>
            </div>
        </div>
    );
}

export default userResult;
