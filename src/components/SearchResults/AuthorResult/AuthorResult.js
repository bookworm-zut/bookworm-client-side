import React from 'react';

import Heading from '../../UI/Text/Heading/Heading';
import Image from '../../UI/Image/Image';

import classes from './AuthorResult.module.css';

const authorResult = props => {
    return (
        <div className={classes.AuthorResult} onClick={props.onClick}>
            <div className={classes.AuthorImage}>
                <Image size="medium" src={props.image} alt="author" />
            </div>
            <div className={classes.AuthorInfo}>
                <Heading size="big">{props.name}</Heading>
                <Heading size="small">{props.birthplace}</Heading>
                <h5>{props.descr}</h5>
            </div>
        </div>
    );
}

export default authorResult;
