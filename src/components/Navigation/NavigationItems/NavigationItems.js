import React from 'react';
import classes from './NavigationItems.module.css';
import NavigationItem from './NavigationItem/NavigationItem';

const navigationItems = props => (
    <ul className={classes.NavigationItems}>
        <NavigationItem clicked={props.clicked} link="/" exact>Home</NavigationItem>
        {!props.isAuthenticated
            ? <NavigationItem clicked={props.clicked} link="/log-in">Log in</NavigationItem>
            : <NavigationItem clicked={props.clicked} link="/log-out">Log out</NavigationItem>}
    </ul>
);

export default navigationItems;
