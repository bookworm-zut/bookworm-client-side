import React from 'react';
import NavigationItems from '../NavigationItems/NavigationItems'
import SearchBar from '../../UI/SearchBar/SearchBar'
import classes from './SideDrawer.module.css'
import Backdrop from '../../UI/Backdrop/Backdrop'
import Aux from '../../../hoc/Auxiliary'
import Image from '../../UI/Image/Image';

import image from '../../../assets/bookworm-logo2.png';

const sideDrawer = (props) => {
    const stateClass = props.show ? classes.Open : classes.Close;
    const classList = [classes.SideDrawer, stateClass]
    return (
        <Aux>
            <Backdrop show={props.show} clicked={props.sideDrawerClosed} />
            <div className={classList.join(' ')}>
                <div className={classes.Logo}>
                    <Image src={image} alt="logo" noshadow />
                </div>
                <SearchBar />
                <nav>
                    <NavigationItems clicked={props.sideDrawerClosed}isAuthenticated={props.isAuth}/>
                </nav>
            </div>
        </Aux>
    )
}

export default sideDrawer;
