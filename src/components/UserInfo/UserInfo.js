import React from 'react';
import classes from './UserInfo.module.css';
import userImage from '../../assets/useravatar2.png';
import Image from '../UI/Image/Image';
import Heading from '../UI/Text/Heading/Heading'

const userInfo = (props) => {
    return (
        <div className={classes.InfoContainer}>
            <Image src={userImage} alt="mickiewicz" round size="large" />
            <Heading size="big">{props.username}</Heading>
            <p>{props.fullname}</p>
            <p>{props.email}</p>
        </div>
    );
}

export default userInfo;
