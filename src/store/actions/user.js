import axios from '../../axios-requests';
import * as actionTypes from './actionTypes';

export const booksFetchStart = () => {
    return {
        type: actionTypes.USER_BOOKS_FETCH_START
    };
};

export const booksFetchSuccess = books => {
    return {
        type: actionTypes.USER_BOOKS_FETCH_SUCCESS,
        books: books
    };
};

export const booksFetchFail = error => {
    return {
        type: actionTypes.USER_BOOKS_FETCH_FAIL,
        error: error
    };
};

export const showcaseFetchStart = () => {
    return {
        type: actionTypes.USER_SHOWCASE_FETCH_START
    };
};

export const showcaseFetchSuccess = showcase => {
    return {
        type: actionTypes.USER_SHOWCASE_FETCH_SUCCESS,
        showcase: showcase
    };
};

export const showcaseFetchFail = error => {
    return {
        type: actionTypes.USER_SHOWCASE_FETCH_FAIL,
        error: error
    };
};

export const ratingsFetchStart = () => {
    return {
        type: actionTypes.USER_RATINGS_FETCH_START
    };
};

export const ratingsFetchSuccess = ratings => {
    return {
        type: actionTypes.USER_RATINGS_FETCH_SUCCESS,
        ratings: ratings
    };
};

export const ratingsFetchFail = error => {
    return {
        type: actionTypes.USER_RATINGS_FETCH_FAIL,
        error: error
    };
};

export const booksFetch = username => {
    return dispatch => {
        dispatch(booksFetchStart());
        axios.get('/getUserTopFive/' + username)
            .then(response => {
                if (response.status === 200 && response.data) {
                    dispatch(booksFetchSuccess(response.data))
                } else {
                    dispatch(booksFetchFail(response.status));
                }
            })
            .catch(error => {
                dispatch(booksFetchFail(error.message));
            });
    };
};

export const ratingsFetch = username => {
    return dispatch => {
        dispatch(ratingsFetchStart());
        axios.get('/getUserRecentRatings/' + username)
            .then(response => {
                if (response.status === 200 && response.data) {
                    dispatch(ratingsFetchSuccess(response.data))
                } else {
                    dispatch(ratingsFetchFail(response.status));
                }
            })
            .catch(error => {
                dispatch(ratingsFetchFail(error.message));
            });
    };
};

export const showcaseFetch = username => {
    return dispatch => {
        dispatch(showcaseFetchStart());
        axios.get('/getUserShowcase/' + username)
            .then(response => {
                if (response.status === 200 && response.data) {
                    dispatch(showcaseFetchSuccess(response.data))
                } else {
                    dispatch(showcaseFetchFail(response.status));
                }
            })
            .catch(error => {
                dispatch(showcaseFetchFail(error.message));
            });
    };
};
