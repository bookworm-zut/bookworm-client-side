import axios from '../../axios-requests';
import * as actionTypes from './actionTypes';

export const fetchBookInfoStart = () => {
    return {
        type: actionTypes.BOOK_INFO_FETCH_START,
    };
};

export const fetchBookInfoFail = (error) => {
    return {
        type: actionTypes.BOOK_INFO_FETCH_FAIL,
        error: error
    };
}

export const fetchBookInfoSuccess = (book) => {
    return {
        type: actionTypes.BOOK_INFO_FETCH_SUCCESS,
        book: book
    };
}

export const fetchReviewsStart = () => {
    return {
        type: actionTypes.BOOK_REVIEWS_FETCH_START,
    };
};

export const fetchReviewsFail = (error) => {
    return {
        type: actionTypes.BOOK_REVIEWS_FETCH_FAIL,
        error: error
    };
}

export const fetchReviewsSuccess = (reviews) => {
    return {
        type: actionTypes.BOOK_REVIEWS_FETCH_SUCCESS,
        reviews: reviews
    };
}

export const fetchBook = (bookName) => {
    return dispatch => {
        dispatch(fetchBookInfo(bookName));
        dispatch(fetchReviews(bookName));
    }
};

export const fetchBookInfo = (bookName) => {
    return dispatch => {
        dispatch(fetchBookInfoStart());
        axios.get(`books/getBookInfo/${bookName}`)
        .then(res => {
            console.log(res.data);
            dispatch(fetchBookInfoSuccess(res.data));
        })
        .catch(err => {
            dispatch(fetchBookInfoFail(err))
        })
    }
}

export const fetchReviews = (bookName) => {
    return dispatch => {
        dispatch(fetchReviewsStart());
        axios.get(`books/getAllBookReviews/${bookName}`)
        .then(res => {
            console.log(res.data);
            dispatch(fetchReviewsSuccess(res.data));
        })
        .catch(err => {
            dispatch(fetchReviewsFail(err))
        })
    }
}