import * as actionTypes from '../actions/actionTypes';

const initialState = {
    book: null,
    reviews: null,
    loadingInfo: true,
    loadingReviews: true
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.BOOK_INFO_FETCH_START:
            return {
                ...state,
                loadingInfo: true
            };
        case actionTypes.BOOK_INFO_FETCH_SUCCESS:
            let rating = action.book.bookAverageRate;
            if (rating === null) {
                rating = "0";
            }
            return {
                ...state,
                book: {
                    id: action.book.id,
                    name: action.book.title,
                    author: action.book.author.name,
                    description: action.book.description,
                    image: action.book.thumbnailUrl,
                    meta: {
                        isbn: action.book.isbn,
                        genre: action.book.categories,
                        rating: parseFloat(action.book.bookAverageRate),
                        //ratingsAmount: parseInt(action.book.meta.ratingsAmount),
                        released: action.book.publishedDate
                    }
                },
                loadingInfo: false
            };
        case actionTypes.BOOK_INFO_FETCH_FAIL:
            return {
                ...state,
                loadingInfo: false
            };
        case actionTypes.BOOK_REVIEWS_FETCH_START:
            return {
                ...state,
                loadingReviews: true
            };
        case actionTypes.BOOK_REVIEWS_FETCH_SUCCESS:
            return {
                ...state,
                reviews: action.reviews,
                loadingReviews: false
            };
        case actionTypes.BOOK_REVIEWS_FETCH_FAIL:
            return {
                ...state,
                loadingReviews: false
            };
        default:
            return state;
    }
}

export default reducer;