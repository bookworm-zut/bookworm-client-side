import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    loading: false,
    category: 'bookName',
    results: [],
    error: null
}

const searchStart = (state, action) => {
    return updateObject(state, {loading: true});
}

const searchSuccess = (state, action) => {
    return updateObject(state, {loading: false, category: action.category, results: action.results});
}

const searchFail = (state, action) => {
    return updateObject(state, {loading: false, results: [], error: action.error});
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SEARCH_START:
            return searchStart(state, action);
        case actionTypes.SEARCH_SUCCESS:
            return searchSuccess(state, action);
        case actionTypes.SEARCH_FAIL:
            return searchFail(state, action);
        default:
            return state;
    }
}

export default reducer;
