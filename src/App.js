import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Layout from './hoc/Layout/Layout';
import Logout from './containers/Auth/Logout/Logout';
import HomePage from './containers/HomePage/HomePage';
import UserProfile from './containers/UserProfile/UserProfile';
import SignUp from './containers/Auth/SignUp/SignUp';
import LogIn from './containers/Auth/LogIn/LogIn';
import ResetPassword from './containers/Auth/ResetPassword/ResetPassword';
import BookPage from './containers/BookPage/BookPage';
import AuthorPage from './containers/AuthorPage/AuthorPage';
import SearchPage from './containers/SearchPage/SearchPage';
import * as actions from './store/actions/';

class App extends Component {
	componentDidMount() {
		this.props.onTryAutoSignup();
	}

	render() {
		/* book and author routes will be dependent on query params */

		let routes = (
			<Switch>
				<Route path="/book/:id" component={BookPage} />
				<Route path="/author/:author" component={AuthorPage} />
				<Route path="/user/:user" component={UserProfile} />
				<Route path="/search" component={SearchPage} />
				<Route path="/reset-password" component={ResetPassword} />
				<Route path="/sign-up" component={SignUp} />
				<Route path="/log-in" component={LogIn} />
				<Route path="/" exact component={HomePage} />
				<Redirect to="/" />
			</Switch>
		);

		if (this.props.isAuthenticated) {
			routes = (
				<Switch>
					<Route path="/book/:id" component={BookPage} />
					<Route path="/author/:author" component={AuthorPage} />
					<Route path="/search" component={SearchPage} />
					<Route path="/log-out" component={Logout} />
					<Route path="/user/:user" component={UserProfile} />
					<Route path="/" exact component={HomePage} />
					<Redirect to="/" />
				</Switch>
			);
		}

		return (
			<div>
				<Layout>
					{routes}
				</Layout>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.auth.isAuthenticated
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onTryAutoSignup: () => dispatch(actions.authCheckState())
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
